from .views import *


def setup_routes(app):
    app.router.add_route('*', '/', index_page)
    app.router.add_get('/api/message/{id}/', get_message)
    app.router.add_post('/api/message/add/', add_message)
