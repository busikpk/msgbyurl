from aiohttp import web
from .models import Message, MessageCollection


async def index_page(request):
    return web.Response(text='index page')


async def add_message(request):
    data = await request.json()
    body = data['body']

    if not body:
        return web.HTTPBadRequest()

    ttl = data.get('ttl', None)
    vtl = data.get('vtl', None)

    message = Message(body, ttl, vtl)
    result = await MessageCollection(request.app['db']).save(message)

    return web.json_response({'data': {'message_id': result.inserted_id}})


async def get_message(request):
    msg_id = request.match_info['id']
    result = await MessageCollection(request.app['db']).get_message(msg_id)
    return web.json_response({'data': result})


