import hashlib
import time
import random
import datetime


class Message(object):

    def __init__(self, body, ttl, vtl):
        self.body = body
        self.ttl = datetime.datetime.fromtimestamp(datetime.datetime.utcnow().timestamp() + ttl)
        self.vtl = vtl

        body_hash = hashlib.sha1(body.encode('utf-8')).hexdigest()
        msg_hash = hashlib.sha1('{}{}{}'.format(time.time(), body_hash, random.random()).encode('utf-8'))

        self.id = '{}'.format(msg_hash.hexdigest())

    def to_dict(self):
        return {
            'body': self.body,
            'ttl': self.ttl,
            'vtl': self.vtl,
            '_id': self.id
        }

    def __str__(self):
        return '{}'.format(self.to_dict())


class MessageCollection(object):

    def __init__(self, connection):
        self._collection = connection['messages']

    async def get_message(self, msg_id):
        result = await self._collection.find_one({"_id": msg_id})
        if result and result['vtl']:
            if datetime.datetime.utcnow() > result['ttl']:
                result = None
            else:
                result['ttl'] = result['ttl'].timestamp()
                await self._collection.update_one({"_id": msg_id}, {"$set": {"vtl": result['vtl'] - 1}})
        else:
            await self._collection.delete_one({"_id": msg_id})
        return result

    async def save(self, msg):
        return await self._collection.insert_one(msg.to_dict())

