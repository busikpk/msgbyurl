import asyncio
from aiohttp import web
from core.routes import setup_routes
from motor.motor_asyncio import AsyncIOMotorClient


def init_db(app):
    db = AsyncIOMotorClient(host='localhost', port=27017)
    # msgs = db['messages_db']['messages']
    # msgs.create_index('ttl', expireAfterSeconds=60)
    app['db'] = db['messages_db']


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    app = web.Application(loop=loop)
    setup_routes(app)
    init_db(app)
    web.run_app(app, host='127.0.0.1', port=8080)